<?php
/**
 * @file
 * Helper functions for the drush recipes webservice features.
 */

/**
 * helper function to return the xml as an array in the DR 1.0 format
 */
function _drush_recipes_service_array_parser($ary, $prefix = '') {
  $arraytmp = array();
  foreach ($ary as $key => $element) {
    $arraytmp[$prefix . $key] = (array) $element;
    $arraytmp[$prefix . $key]['metadata'] = array();
    $recipe_file = NULL;
    // test for file upload based recipes
    $test = (array) $element;
    if (isset($test['drecipe_file'])) {
      $machine_name = $test['name'];
      $item = $test['drecipe_file'];
      $recipe_file = _drush_recipes_service_load_recipe($item);
      $tmprecipe = array();
      foreach ($recipe_file['recipe'] as $k1 => $l1) {
        $tmpkey = $k1;
        if (is_int($tmpkey)) {
          $tmpkey = 'arg' . $k1;
        }
        if (is_array($l1)) {
          foreach ($l1 as $k2 => $l2) {
            $tmpkey2 = $k2;
            if (is_int($tmpkey2)) {
              $tmpkey2 = 'arg' . $k2;
            }

            if (is_array($l2)) {
              foreach ($l2 as $k3 => $l3) {
                $tmpkey3 = $k3;
                if (is_int($tmpkey3)) {
                  $tmpkey3 = 'arg' . $k3;
                }
                $tmpkey3 = str_replace('[', '', $tmpkey3);
                $tmpkey3 = str_replace(']', '', $tmpkey3);
                $tmprecipe[$tmpkey][$tmpkey2][$tmpkey3] = $l3;
              }
            }
            else {
              $tmprecipe[$tmpkey][$tmpkey2] = $l2;
            }
          }
        }
        else {
          $tmprecipe[$tmpkey] = $l1;
        }
      }
      $arraytmp[$prefix . $key] = $recipe_file;
      $arraytmp[$prefix . $key]['recipe'] = $tmprecipe;
      $machine_name = strtolower($machine_name);
      // check for spaces and replace with underscores.
      $machine_name = preg_replace("/[^a-z0-9_]/" , '_', $machine_name);
      $arraytmp[$prefix . $key]['machine_name'] = $machine_name;
    }
    else {
      foreach ($element as $key2 => $item) {
        $item = (count((array) $item) == 1 ? (string) $item : (array) $item);
        // recipe is the only thing that drills down
        if ($key2 == 'recipe') {
          $arraytmp[$prefix . $key]['recipe'] = array();
          $calls = explode("\n", trim($item));
          foreach ($calls as $key3 => $drushcall) {
            $arraytmp[$prefix . $key]['recipe']["call$key3"] = array();
            $parts = explode('  ', trim($drushcall));
            foreach ($parts as $key4 => $part) {
              $arraytmp[$prefix . $key]['recipe']["call$key3"]["arg$key4"] = $part;
            }
          }
        }
        else {
          // meta data properties
          if (in_array($key2, array('created', 'author', 'type', 'description', 'version', 'image'))) {
            $arraytmp[$prefix . $key]['metadata'][$key2] = $item;
            unset($arraytmp[$prefix . $key][$key2]);
          }
          else {
            $arraytmp[$prefix . $key][$key2] = $item;
          }
        }
      }
    }
  }
  return $arraytmp;
}

/**
 * Helper function to convert array to XML
 * @param  array $array any old array
 * @param  object $xml   SimpleXMLElement object type
 * @return object        returns a populated XML object with values from array
 */
function _drush_recipes_service_array_to_xml($array, &$xml) {
  foreach($array as $key => $value) {
    if(is_array($value)) {
      if(!is_numeric($key)){
        $subnode = $xml->addChild("$key");
        _drush_recipes_service_array_to_xml($value, $subnode);
      }
      else {
        _drush_recipes_service_array_to_xml($value, $xml);
      }
    }
    else {
        $xml->addChild("$key","$value");
    }
  }
}

/**
 * Helper function to load a file based on drush call
 * @param  string  $path     path to a recipe to load
 * @param  boolean $validate whether this is part of validating the recipe spec
 * @return array decoded array from one of the formats listed.
 */
function _drush_recipes_service_load_recipe($path, $validate = FALSE) {
  // if we are attempting a validation we just want to try and load an item
  // back and assert that it is the same as what was passed in
  if ($validate) {
    $contents = $path;
  }
  else {
    // loading as usual, this is a recipe file
    $contents = @file_get_contents($path);
    // ensure utf8 to avoid issues
    $contents = utf8_encode($contents);
  }
  // to avoid issues w/ format extension names we trickle down based on support
  $recipe = drupal_json_decode($contents);
  // if empty, lets try a yaml parser
  if (empty($recipe) && function_exists('yaml_parse')) {
    // test for yaml capabilities
    $recipe = yaml_parse($contents);
  }
  // lets try xml as a last ditch
  if (empty($recipe)) {
    $recipe = simplexml_load_string($contents);
  }
  // if still empty now we have a REAL problem
  if (empty($recipe)) {
    // something went wrong, we couldn't read the file
    //drush_log(dt('There is a problem with the recipe located at @file', array('@file' => $path)), 'error');
    return FALSE;
  }
  return $recipe;
}
